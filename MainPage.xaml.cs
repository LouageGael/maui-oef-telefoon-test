﻿namespace TelefoonMaui;

public partial class MainPage : ContentPage
{
    private Telefoon telefoon = new Telefoon();
    private TelefoonTweedeGeneratie telefoonTweedeGeneratie = new TelefoonTweedeGeneratie();
    private LuxeTelefoon luxeTelefoon = new LuxeTelefoon();
    public MainPage()
    {
        InitializeComponent();
        rdbDraaiTelefoon.IsChecked = true;
        TagsActive(false, false, false, false, false);
    }
    private void OnbtnBellenClicked(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtBelEenNummer.Text))
        {
            if (txtBelEenNummer.Text.All(char.IsDigit))
            {
                if ((bool)rdbDraaiTelefoon.IsChecked)
                {
                    telefoon.Bellen(txtBelEenNummer.Text);
                }
                else if ((bool)rdbToetsTelefoon.IsChecked)
                {
                    telefoonTweedeGeneratie.Bellen(txtBelEenNummer.Text);
                }
                else if ((bool)rdbLuxeTelefoon.IsChecked)
                {
                    luxeTelefoon.Bellen(txtBelEenNummer.Text);
                    pickerHistoriek.Items.Clear();
                    foreach (var item in luxeTelefoon.LijstHistoriek)
                    {
                        pickerHistoriek.Items.Add(item);
                    }

                }
                lblBellen.Text = telefoon.Bellen(txtBelEenNummer.Text);
            }
            else
            {
                lblBellen.Text = "Nummer moet numeriek zijn.";
            }
        }
        else
        {
            lblBellen.Text = "Geen nummer ingegeven.";
        }
    }
    private void OnbtnCantactToevoegenClicked(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtNaam.Text) && !string.IsNullOrEmpty(txtNummer.Text))
        {
            if ((bool)rdbLuxeTelefoon.IsChecked)
            {

                luxeTelefoon.LijstNummers.Add(new Contact()
                {
                    Nummer = txtNummer.Text,
                    Name = txtNaam.Text
                });
               
                lvContact.ItemsSource = null;
                lvContact.ItemsSource = luxeTelefoon.LijstNummers;
            }
            else if ((bool)rdbToetsTelefoon.IsChecked)
            {

                telefoonTweedeGeneratie.LijstNummers.Add(new Contact()
                {
                    Nummer = txtNummer.Text,
                    Name = txtNaam.Text
                });
                lvContact.ItemsSource = null;
                lvContact.ItemsSource = telefoonTweedeGeneratie.LijstNummers;


            }

            txtNaam.Text = string.Empty;
            txtNummer.Text = string.Empty;
        }
    }


    // sluiten
    private void OnBtnSluitenClicked(object sender, EventArgs e)
    {
        Environment.Exit(0);
    }
    // checkboxes
    private void OnrdbDraaiTelefoonCheckedChanged(object sender, CheckedChangedEventArgs e)
    {
        telefoon = new Telefoon();
        TagsActive(false, false, false, false, false);
    }
    private void OnrdbLuxeTelefoonCheckedChanged(object sender, CheckedChangedEventArgs e)
    {
        lvContact.ItemsSource = null;
        lvContact.ItemsSource = luxeTelefoon.LijstNummers;
        TagsActive(true, true, true, true, true);
    }
    private void OnrdbToetsTelefoonCheckedChanged(object sender, CheckedChangedEventArgs e)
    {
        lvContact.ItemsSource = null;
        lvContact.ItemsSource = telefoonTweedeGeneratie.LijstNummers;
        TagsActive(true, true, true, true, false);
    }
    private void TagsActive(bool naam, bool nummer, bool contact, bool contacten, bool historiek)
    {
        lblBellen.Text = String.Empty;
        txtNaam.IsEnabled = naam;
        txtNummer.IsEnabled = nummer;
        btnContactToevoegen.IsEnabled = contact;
        pickerHistoriek.IsEnabled = historiek;
    }
}
class Telefoon
{
    private string Ring(string nummer) => "Ring ring naar " + nummer;
    public virtual string Bellen(string nummer) => Ring(nummer);
}
class TelefoonTweedeGeneratie : Telefoon
{
    public override string Bellen(string nummer) => "";
    public string Bellen(int indexNr) => base.Bellen(LijstNummers[indexNr].Nummer);
    public void AddNummer(Contact contact)
    {
        if (LijstNummers.Count <= 10)
        {
            LijstNummers.Add(contact);
        }
    }

    public List<Contact> LijstNummers { get; set; } = new List<Contact>();
}

class LuxeTelefoon : TelefoonTweedeGeneratie
{
    public List<string> LijstHistoriek { get; set; } = new List<string>();

    private void AddHistoriek(string nummer)
    {
        if (LijstHistoriek.Count < 5)
        {
            LijstHistoriek.Add(nummer);
        }
    }
    //public string Bellen(bool historiek, int indexNr)
    //{
    //    if (historiek)
    //    {
    //        return LijstHistoriek.ElementAt(indexNr);
    //    }
    //    return "Geen nummer opgeslagen op deze index";
    //}
    public override string Bellen(string nummer)
    {
        AddHistoriek(nummer);
        return base.Bellen(nummer);
    }
}
class Contact
{
    public string Name { get; set; }
    public string Nummer { get; set; }
}
